local sensorInfo = {
	name = "MetalSpotsOccupation",
	desc = "Return data about metal extractors occupation per metal spot",
	author = "PepeAmpere",
	date = "2017-04-21",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- TEMP
local MEX_DEFID = UnitDefNames["armmex"].id

-- @description return occupation of the mexes
return function(metalSpots)
	local empty = 0
	local emptyPositions = {}
	local blocking = {}
	local occupied = 0
	for i=1, #metalSpots do
		local thisSpotPos = metalSpots[i]
		local blockingSpot = Spring.TestBuildOrder(MEX_DEFID, thisSpotPos.x, thisSpotPos.y, thisSpotPos.z, 1)
		if (blockingSpot > 0) then			
			empty = empty + 1
			emptyPositions[empty] = thisSpotPos
			blocking[empty] = blockingSpot
		else
			occupied = occupied + 1
		end
	end
	
	local finalData = {
		empty = empty,
		emptyPositions = emptyPositions,
		blocking = blocking,
		occupied = occupied,
	}
	return finalData
end