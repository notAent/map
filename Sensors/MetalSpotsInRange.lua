local sensorInfo = {
	name = "MetalSpotsInRange",
	desc = "Return metal spots in certain radius",
	author = "PepeAmpere",
	date = "2017-04-25",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- TEMP

-- @description return occupation of the mexes
return function(metalSpots, center, radius)
	local newMetalSpots = {}
	for i=1, #metalSpots do
		local thisSpotPos = metalSpots[i]
		local distance = center:Distance(thisSpotPos)
		if (distance < radius) then		
			newMetalSpots[#newMetalSpots + 1] = thisSpotPos
		end
	end
	return newMetalSpots
end