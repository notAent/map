local sensorInfo = {
	name = "BuildSpotUnoccupied",
	desc = "Checks if given buildSpot is still unoccupied.",
	author = "PepeAmpere",
	date = "2017-05-08",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local abs = math.abs

-- @description return if spot is valid
-- @argument buildSpot [table] structure describing buildspot, his search criteria and planned building
-- @example buildSpot [table] example 
-- {
	-- sizeX = 8,
	-- sizeX = 8,
	-- position = Vec3(-1328, 0, -581),
	-- score = 100,
	-- buildingDefID = 8,
	-- buildingName = "armmex",
-- }
-- @return bestSpot 
return function(buildSpot)
	-- Spring.TestBuildOrder
	-- ( number unitDefID, number x, number y, number z, number facing)
	-- -> number blocking [,number featureID]
	-- blocking can be:
	-- 0 - blocked
	-- 1 - mobile unit in the way
	-- 2 - free  (or if featureID is != nil then with a blocking feature that can be reclaimed)
	local blocking, featureID = Spring.TestBuildOrder(buildSpot.buildingDefID, buildSpot.position.x, buildSpot.position.y, buildSpot.position.z, 1)		
	
	if (blocking > 0) then
		return true
	else
		return false
	end
end