local sensorInfo = {
	name = "HPInAreaEnemyThreshold",
	desc = "Return T/F based on if the enemy HP in given area have reached some level.",
	author = "PepeAmpere",
	date = "2017-06-12",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- this sensor is not caching any values, it evaluates itself on every request

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description Return if enemy HP count have reached given threshold in given area
-- @argument area [table] {center = Vec3, radius = number} 
-- @argument threshold [number] total number of enemy HP
-- @comment sensor data are limited by your team knowledge (LOS, radar coverage, etc.)
return function(area, threshold)
	if (threshold == nil) then threshold = 0 end
	
	-- get HP count
	local areaHPs = Sensors.map.HPinArea(area)
	
	-- return answer for the question
	return areaHPs.enemy >= threshold
end