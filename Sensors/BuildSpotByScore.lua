local sensorInfo = {
	name = "BuildSpotByScore",
	desc = "Based on some scoring logic it will return the best spot for given building from passed buildSpots map.",
	author = "PepeAmpere",
	date = "2017-04-21",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching
local FIRST_SEARCH_LIMIT = 30 -- how many unblocked positions is our pool for search in scores
local SCORE_INIT = 100
local SCORE_PERFECT_MATCH_MULT = 2
local SCORE_RECLAIM = -((SCORE_INIT / 5) * 2) -- -40
local SCORE_MOBILE = -(SCORE_INIT / 5) -- -20
 
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local abs = math.abs

-- @description return available spaces for building
-- @argument buildSpots [array of tables] spots mask in next format { {sizeX = <number>, sizeZ = <number>, position = <Vec3 relative to center position>}, ... {...} }
-- @argument buildingName [string] name of the building we want to fit into mask
-- @example input of buildSpots
-- {
--		{ sizeX = 10,  sizeZ = 10, position = Vec3(120, 0, 67)},
--		{ sizeX = 8,  sizeZ = 8, position = Vec3(-1328, 0, -581)},
-- }
-- @return bestSpot [table] example { sizeX = 8,  sizeZ = 8, position = Vec3(-1328, 0, -581)}
return function(buildSpots, buildingName)
	if (buildSpots == nil or #buildSpots < 1) then return end
	local buildingDefID = UnitDefNames[buildingName].id
	local buildingSizeX = UnitDefNames[buildingName].xsize
	local buildingSizeZ = UnitDefNames[buildingName].zsize
	local bestSpot = {}
	local foundSpots = {}
	local foundSpotsCount = 0
	local foundSpotsScore = {}
	local buildSpotsCount = #buildSpots
	local randomAccess = math.random(buildSpotsCount)
	local SpringTestBuildOrder = Spring.TestBuildOrder
	
	for i=randomAccess, buildSpotsCount + randomAccess do
		local index = (i % buildSpotsCount) + 1
		local thisSpot = buildSpots[index]
		
		local thisSpot = buildSpots[index]
		
		-- Spring.TestBuildOrder
		-- ( number unitDefID, number x, number y, number z, number facing)
		-- -> number blocking [,number featureID]
		-- blocking can be:
		-- 0 - blocked
		-- 1 - mobile unit in the way
		-- 2 - free  (or if featureID is != nil then with a blocking feature that can be reclaimed)		
		local blocking, featureID = SpringTestBuildOrder(buildingDefID, thisSpot.position.x, thisSpot.position.y, thisSpot.position.z, 1)		
		if (blocking > 0) then
			foundSpotsCount = foundSpotsCount + 1
			foundSpots[foundSpotsCount] = buildSpots[index]
			
			-- SCORING LOGIC
			-- init score
			local score = SCORE_INIT
			
			-- size match penalties
			-- perfect match
			if (buildingSizeX == thisSpot.sizeX and buildingSizeZ == thisSpot.sizeZ) then
				score = score * SCORE_PERFECT_MATCH_MULT
			end
			
			-- decrease if X or Z dont match
			score = score - abs(buildingSizeX - thisSpot.sizeX)
			score = score - abs(buildingSizeZ - thisSpot.sizeZ)
						
			-- decrease if blocked
			if (blocking == 1) then 
				if (featureID ~= nil) then -- reaclaim is penalized more
					score = score + SCORE_RECLAIM
				else -- mobile units penalize as well
					score = score + SCORE_MOBILE
				end
			end
			
			-- tbd add bonuses/penalties for clusterring
			
			-- save it
			foundSpotsScore[#foundSpotsScore + 1] = score
		end
		
		if foundSpotsCount > FIRST_SEARCH_LIMIT then
			break
		end
	end
	
	local lastBestScore = -math.huge
	for i=1, foundSpotsCount do
		local thisFoundSpotScore = foundSpotsScore[i]
		if thisFoundSpotScore > lastBestScore then
			bestSpot = foundSpots[i]
			lastBestScore = thisFoundSpotScore
		end
	end
	
	return {
		sizeX = bestSpot.sizeX,
		sizeX = bestSpot.sizeZ,
		position = bestSpot.position,
		score = lastBestScore,
		buildingDefID = buildingDefID,
		buildingName = buildingName,
	}
end