local sensorInfo = {
	name = "BuildSpotsFromMask",
	desc = "Return build spots structure based on passed mask",
	author = "PepeAmpere",
	date = "2017-05-08",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local abs = math.abs

-- @description return available spaces for building
-- @argument buildMask [array of tables] spots mask in next format { {sizeX = <number>, sizeZ = <number>, position = <Vec3 relative to center position>}, ... {...} }
-- @argument centerPosition [Vec3] center for the mask relative positions
-- @example input of buildMask
-- {
--		{ sizeX = 10,  sizeZ = 10, position = Vec3(120, 0, 67)},
--		{ sizeX = 8,  sizeZ = 8, position = Vec3(-1328, 0, -581)},
-- }
-- @return buildSpots [array of tables]
-- @example of buildSpots
-- {
--		{ sizeX = 10,  sizeZ = 10, position = Vec3(120, 0, 67), lastScore = 0, lastUpdate = {score = 0, blocking = 0}},
--		{ sizeX = 8,  sizeZ = 8, position = Vec3(-1328, -40, -581), lastScore = 0, lastUpdate = {score = 0, blocking = 1, featureID = 36}},
-- }
return function(buildMask, centerPosition)
	local buildSpots = {}
	local buildSpotsCounter = 0
	for i=1, #buildMask do
		local thisMaskSpot = buildMask[i]
		-- TBD: add heights filtering
		
		buildSpotsCounter = buildSpotsCounter + 1
		buildSpots[buildSpotsCounter] = {
			sizeX = thisMaskSpot.sizeX,
			sizeZ = thisMaskSpot.sizeZ,
			position = centerPosition + thisMaskSpot.position, -- now the position is no longer relative, directly pointing to some absolute pos
			lastUpdate = {
				blocking = 0,
			},
		}
	end
	
	return buildSpots 
end