map 0.4
====

*(not officially released, yet)*

Sensors
---

* BuildSpots
    * BuildSpotByScore
    * BuildSpotsFromMask
    * BuildSpotUnoccupied
* Environment
    * Wind
* Metal
    * MetalSpots (!temporary dependency on internal nota archive method to get metal spots, will be replaced later)
    * MetalSpotsInRange
    * MetalSpotsOccupation
* Tactical
	* HPinArea
	* HPinAreaEnemyThreshold
    * PrivateThreatAreasByHP
    * PrivateThreatAreasByHPUpdate
    * TacticalAdvantageByHP
    * TacticalAdvantageByHPForGroundAttack
